-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:8889
-- Généré le :  Lun 25 Avril 2016 à 20:38
-- Version du serveur :  5.5.38
-- Version de PHP :  5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `RentCar_2016_project`
--

-- --------------------------------------------------------

--
-- Structure de la table `Admin`
--

CREATE TABLE `Admin` (
`ID` int(11) NOT NULL,
  `pseudo` text NOT NULL,
  `mot_de_passe` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Admin`
--

INSERT INTO `Admin` (`ID`, `pseudo`, `mot_de_passe`) VALUES
(1, 'Admin', '1234'),
(2, 'Mysql_TEST', '1001');

-- --------------------------------------------------------

--
-- Structure de la table `Client`
--

CREATE TABLE `Client` (
`ID` int(11) NOT NULL,
  `Nom` text NOT NULL,
  `Prenom` text NOT NULL,
  `Adresse_Rue` text NOT NULL,
  `Adresse_CP` text NOT NULL,
  `Adresse_Ville` text NOT NULL,
  `pseudo` text,
  `mot_de_passe` text
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Client`
--

INSERT INTO `Client` (`ID`, `Nom`, `Prenom`, `Adresse_Rue`, `Adresse_CP`, `Adresse_Ville`, `pseudo`, `mot_de_passe`) VALUES
(1, 'd''Aboville', 'Audouin', 'Frégate Est 4 ', '97240', 'le François', 'null', 'null'),
(2, 'Closset', 'Manon', 'Alesia', '75001', 'Paris', 'null', 'null'),
(3, 'Bestaoui', 'Sarah', '45 Rue', '75001', 'Paris', 'null', 'null'),
(4, 'Guedj', 'Alexandre', '34 Rue', '75001', 'Paris', 'null', 'null'),
(5, 'Gallet', 'Antoine', 'Fregate Est 4', '97240', 'Martinique', 'null', 'null'),
(6, 'de Gentil', 'Constance', '12 rue dialou', '75002', 'Paris', 'null', 'null');

-- --------------------------------------------------------

--
-- Structure de la table `Location`
--

CREATE TABLE `Location` (
`ID` int(11) NOT NULL,
  `numero` text NOT NULL,
  `debut` text NOT NULL,
  `fin` text NOT NULL,
  `Vehicule_ID` text NOT NULL,
  `prix_periode` text NOT NULL,
  `assurance` text NOT NULL,
  `paye` text NOT NULL,
  `ClientID` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Location`
--

INSERT INTO `Location` (`ID`, `numero`, `debut`, `fin`, `Vehicule_ID`, `prix_periode`, `assurance`, `paye`, `ClientID`) VALUES
(3, '0001', '1/10/2016', '10/10/2016', '3', '10', 'false', 'true', '1');

-- --------------------------------------------------------

--
-- Structure de la table `Vehicule`
--

CREATE TABLE `Vehicule` (
`ID` int(11) NOT NULL,
  `Marque` text NOT NULL,
  `Moto` text NOT NULL,
  `Auto` text NOT NULL,
  `Modele` text NOT NULL,
  `Cylindre` text NOT NULL,
  `Immatriculation` text NOT NULL,
  `Kilometrage` text NOT NULL,
  `Disponibilite` tinyint(1) NOT NULL,
  `Carburant_level` text NOT NULL,
  `etat` text NOT NULL COMMENT '0(bad) - 100(good)',
  `TypeClient` text NOT NULL COMMENT '(Luxe ou standard)',
  `Prix` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Vehicule`
--

INSERT INTO `Vehicule` (`ID`, `Marque`, `Moto`, `Auto`, `Modele`, `Cylindre`, `Immatriculation`, `Kilometrage`, `Disponibilite`, `Carburant_level`, `etat`, `TypeClient`, `Prix`) VALUES
(3, 'Ford', '0', '1', 'Mustang', '12000', '23EF46', '10', 0, '100', '100', '1', '10'),
(6, 'Ford', '0', '1', 'Fiesta', '10', '34EFR56', '0', 1, '100', '100', '2', '10'),
(7, 'Ferrari', '0', '1', 'California', '1000', 'ZDG73J3', '110', 0, '90', '100', '1', '100'),
(8, 'Renaud', '0', '1', 'Laguna', '100', '3EGFB474', '10000', 1, '100', '80', '2', '30'),
(9, 'BMW', '0', '1', 'Modele 6', '100', '389YEB48B', '0', 1, '100', '100', '1', '120'),
(10, 'Tesla', '0', '1', 'Modèle S', '250', '2UG3V84', '10', 1, '100', '100', '1', '300');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Admin`
--
ALTER TABLE `Admin`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `Client`
--
ALTER TABLE `Client`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `Location`
--
ALTER TABLE `Location`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `Vehicule`
--
ALTER TABLE `Vehicule`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Admin`
--
ALTER TABLE `Admin`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `Client`
--
ALTER TABLE `Client`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `Location`
--
ALTER TABLE `Location`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `Vehicule`
--
ALTER TABLE `Vehicule`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;