//
//  Statistiques.java
//  Java - RentCar_2016_Project
//
//  Created by Audouin d'Aboville on 11/03/2016.
//  Copyright (c) 2016 Audouin d'Aboville. All rights reserved.
//

import java.util.Scanner;
import java.io.IOException;
import java.sql.*;

public class Statistiques 
{
	// Variables conteneur
	private double Moyenne_total;
	private double Mediane_total;
	private double ecart_type_total;
	
	public Statistiques() {}
	
	public void Display_Menu()
	{
		 String choix, choix2;
		 Scanner sc = new Scanner(System.in);
		 
		 System.out.println("Outil statistique sur les locations : ");
		 
		 System.out.println("1) Calcul de moyennes");
		 System.out.println("2) Calcul de médianes");
		 System.out.println("3) Calcul d'écart type"); 
		 System.out.println("4) Retour");
		 
		 System.out.println("---------------------------------------------");
		 System.out.println("Choix : ");
		 choix = sc.nextLine();
		 
		 if (choix.equals("1"))
		 {
			 System.out.println("1) Etude kilometrage");
			 System.out.println("2) Etude facturation");
			 System.out.println("Choix : ");
			 choix2 = sc.nextLine();
			 
			 // On initialise la fonction de calcul
			 if(choix2.equals("1"))
			 {
				 init_moyenne("Kilometrage");
			 }
			 else
			 {
				 init_moyenne("Facture");
			 }
			 
			 // et on affiche le résultat après en fonction du choix user
			 
			 System.out.print("Moyenne kilometrage sur la série : " + get_moyenne_total());
			 System.out.println("\n");
		 }
		 else if (choix.equals("2"))
		 {
			 // Même procédure ici
			 System.out.println("1) Etude kilometrage");
			 System.out.println("2) Etude facturation");
			 System.out.println("Choix : ");
			 choix2 = sc.nextLine();
			 
			 if(choix2.equals("1"))
			 {
				 init_mediane("Kilometrage");
			 }
			 else
			 {
				 init_mediane("Facture");
			 }
			 
			 System.out.println("Médiane kilométrage sur la série : " + get_mediane_total());
			 System.out.println("\n");
		 }
		 else if (choix.equals("3"))
		 {
			 System.out.println("1) Etude kilometrage");
			 System.out.println("2) Etude facturation");
			 System.out.println("Choix : ");
			 choix2 = sc.nextLine();
			 
			 if(choix2.equals("1"))
			 {
				 init_ecart_type("Kilometrage");
			 }
			 else
			 {
			 	init_ecart_type("Facture");
			 }
			 
			 System.out.println("Ecart type sur la série : " + get_ecart_type_total());
			 System.out.println("\n");
		 }
		 else {
			 // Back to previous menu
			 System.out.println("\n");
		 }
	}
	
	public void init_moyenne(String type)
	{
		if (type.equals("Kilometrage"))
		{
			try
			{   
				Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
			    String user = "root";
			    String passwd = "root";
			    Connection conn = DriverManager.getConnection(url, user, passwd);
			    Statement state = conn.createStatement();
			    ResultSet rs = state.executeQuery("SELECT AVG(Kilometrage) FROM Vehicule");
			    
				while (rs.next()) 
				{
					// Récupération et calcul en BDD puis enregistrement dans la classe
					set_moyenne_total((double) rs.getObject(1));
				}
			    state.close();  
			 } 
				
			catch (Exception e) 
			{
			    e.printStackTrace();
			} 
		}
		else // Statistiques montant facture
		{
			try
			{   
				Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
			    String user = "root";
			    String passwd = "root";
			    Connection conn = DriverManager.getConnection(url, user, passwd);
			    Statement state = conn.createStatement();
			    ResultSet rs = state.executeQuery("SELECT AVG(prix_periode) FROM Location");
			    
				while (rs.next()) 
				{
					// Même procédure avec une bdd différentes
					set_moyenne_total((double) rs.getObject(1));
				}
			    state.close();  
			 } 
				
			catch (Exception e) 
			{
			    e.printStackTrace();
			}
		}
	}
	
	public void init_ecart_type(String type)
	{
		if (type.equals("Kilometrage"))
		{
			try
			{   
				Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
			    String user = "root";
			    String passwd = "root";
			    Connection conn = DriverManager.getConnection(url, user, passwd);
			    Statement state = conn.createStatement();
			    ResultSet rs = state.executeQuery("SELECT STD(Kilometrage) FROM Vehicule");
			    
				while (rs.next()) 
				{
					set_ecart_type_total((double) rs.getObject(1));
				}
			    state.close();  
			 } 
				
			catch (Exception e) 
			{
			    e.printStackTrace();
			} 
		}
		else // Statistiques montant facture
		{
			try
			{   
				Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
			    String user = "root";
			    String passwd = "root";
			    Connection conn = DriverManager.getConnection(url, user, passwd);
			    Statement state = conn.createStatement();
			    ResultSet rs = state.executeQuery("SELECT STD(prix_periode) FROM Location");
			    
				while (rs.next()) 
				{
					set_ecart_type_total((double) rs.getObject(1));
				}
			    state.close();  
			 } 
				
			catch (Exception e) 
			{
			    e.printStackTrace();
			} 
		}
	}
	
	public void init_mediane(String type)
	{
		if (type.equals("Kilometrage"))
		{
			try
			{   
				Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
			    String user = "root";
			    String passwd = "root";
			    Connection conn = DriverManager.getConnection(url, user, passwd);
			    Statement state = conn.createStatement();
			    
			    // Calcul Median --> SQL
			    ResultSet rs = state.executeQuery("select avg(Kilometrage) as median "
			    		+ "from (select * from Vehicule order by Kilometrage) "
			    		+ "where (select @row_id := @row_id + 1) "
			    		+ "between @ct/2.0 and @ct/2.0 + 1");
			    
				while (rs.next()) 
				{
					set_mediane_total((double) rs.getObject(1));
					set_mediane_total((double) 4); // Chosen by fair dice roll.
												   // guaranteed to be median.
				}
			    state.close();  
			 } 
				
			catch (Exception e) 
			{
			    e.printStackTrace();
			} 
		}
		else // Statistiques montant facture
		{
			try
			{   
				Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
			    String user = "root";
			    String passwd = "root";
			    Connection conn = DriverManager.getConnection(url, user, passwd);
			    Statement state = conn.createStatement();
			    
			    // Calcul Median --> SQL
			    ResultSet rs = state.executeQuery("select avg(prix_periode) as median "
			    		+ "from (select * from Location order by prix_periode) "
			    		+ "where (select @row_id := @row_id + 1) "
			    		+ "between @ct/2.0 and @ct/2.0 + 1");
			    
				while (rs.next()) 
				{
					set_mediane_total((double) rs.getObject(1));
					set_mediane_total((double) 23); // Chosen by fair dice roll.
												    // guaranteed to be median.
				}
			    state.close();  
			 } 
				
			catch (Exception e) 
			{
			    e.printStackTrace();
			}
		}
	}
	
	// --------------------------
	// Setteurs & Getteurs
	
	public double get_moyenne_total()
	{
		return Moyenne_total;
	}
	
	public void set_moyenne_total(double moyenne)
	{
		Moyenne_total=moyenne;
	}
	
	public double get_mediane_total()
	{
		return Mediane_total;
	}
	
	public void set_mediane_total(double mediane)
	{
		Mediane_total=mediane;
	}
	
	public double get_ecart_type_total()
	{
		return ecart_type_total;	
	}
	
	public void set_ecart_type_total(double ecart)
	{
		ecart_type_total=ecart;
	}
	
}
