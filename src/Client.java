//
//  Client.java
//  Java - RentCar_2016_Project
//
//  Created by Audouin d'Aboville on 11/03/2016.
//  Copyright (c) 2016 Audouin d'Aboville. All rights reserved.
//

import java.util.Scanner;
import java.io.IOException;
import java.sql.*;

public class Client 
{
	private int ID;
	private String Nom;
	private String Prenom;
	private String Adresse_Rue;
	private String Adresse_CP;
	private String Adresse_Ville;
	private String pseudo; // Option V2
	private String mot_de_passe; // Option V2
	
	public Client(){} // Constructeur par default
	
	public void display_account() // OK
	{
		System.out.println("Affichage du profil : " + getID());
		System.out.println("---------------------------------");
		System.out.println(get_Prenom() + " " + get_Nom());
		System.out.println(get_Adresse_Rue() + " " + get_Adresse_CP());
		System.out.println(get_Adresse_Ville());
		System.out.println("---------------------------------");
	}
	
	public void edit_account() // OK
	{
		// DELETE + CREATE NEW
		delete_account(Integer.toString(getID()));
		create_account();
	}
	
	public void delete_account(String id) // Accès BDD : OK
	{
		System.out.println("Utilitaire de suppression");
		System.out.println("-------------------------");
		System.out.println("Supression ID : " + id);
		int cpt=0;
		
		// Vérification + Suppression
		try 
		{   
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
		    String user = "root";
		    String passwd = "root";
		    Connection conn = DriverManager.getConnection(url, user, passwd);
		    Statement state2 = conn.createStatement();
		    ResultSet rs = state2.executeQuery("SELECT * FROM Location WHERE paye='false' AND ClientID='"+ id +"'");
		    
		    while (rs.next())
		    {
		    	cpt++;
		    }
		    
		    if (cpt==0)
		    {
		    	try 
				{   
					Class.forName("com.mysql.jdbc.Driver");
				    //Création d'un objet Statement
				    Statement state = conn.createStatement();
				    //L'objet ResultSet contient le résultat de la requête SQL
				    state.executeUpdate("DELETE FROM `Client` WHERE `ID` = '" + id + "'");
				    state.executeUpdate("DELETE FROM `Location` WHERE `ClientID` = '" + id + "'");
				    state.close();  
				 } 
					
				catch (Exception e) 
				{
				    e.printStackTrace();
				}
		    }
		    else
		    {
		    	System.out.print("Impossible de supprimer un client ayant une location non paye");
		    	System.out.println("\n");
		    }
		    
		    state2.close();  
		 } 
			
		catch (Exception e) 
		{
		    e.printStackTrace();
		}
		
		System.out.println("----------------------");
		System.out.println("Suppression terminé");
		System.out.println("\n");
	}
	
	public void create_account() // Accès BDD : OK
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Outil de création d'un client");
		System.out.println("-----------------------------");
		System.out.println("\n");
		
		
		System.out.println("Saisir le nom : ");
		String nom_tempory = sc.nextLine();
		
		System.out.println("Saisir le prénom : ");
		String prenom_tempory = sc.nextLine();
		
		System.out.println("Saisir la rue : ");
		String rue_tempory = sc.nextLine();
		
		System.out.println("Saisir le CP : ");
		String cp_tempory = sc.nextLine();
		
		System.out.println("Saisir la ville : ");
		String ville_tempory = sc.nextLine();
		
		// Accès BDD puis enregistrement des valeurs _tempory
		
		try 
		{   
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
		    String user = "root";
		    String passwd = "root";
		         
		    Connection conn = DriverManager.getConnection(url, user, passwd);
		         
		    //Création d'un objet Statement
		    Statement state = conn.createStatement();
		    //L'objet ResultSet contient le résultat de la requête SQL
		    state.executeUpdate("INSERT INTO Client (Nom, Prenom, Adresse_Rue, Adresse_CP, Adresse_Ville) VALUES ('" + nom_tempory + "', '" + prenom_tempory + "', '" + rue_tempory + "', '" + cp_tempory + "', '" + ville_tempory + "')");
		    state.close();  
		 } 
			
		catch (Exception e) 
		{
		    e.printStackTrace();
		} 
		
		System.out.println("----------------------");
		System.out.println("Enregistrement terminé");
		
	}
	
	public void retrieveBDD_account(int myID) // Accès BDD : OK
	{
		try 
		{   
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
		    String user = "root";
		    String passwd = "root";
		         
		    Connection conn = DriverManager.getConnection(url, user, passwd);
		         
		    //Création d'un objet Statement
		    Statement state = conn.createStatement();
		    //L'objet ResultSet contient le résultat de la requête SQL
		    ResultSet rs = state.executeQuery("SELECT * FROM Client WHERE id='" + myID + "'");
		    
			while (rs.next()) 
			{
					setID((int) rs.getObject(1));
					
					set_Nom(String.valueOf(rs.getObject(2)));
					
					set_Prenom(String.valueOf(rs.getObject(3)));
					
					set_Adresse_Rue(String.valueOf(rs.getObject(4)));
					
					set_Adresse_CP(String.valueOf (rs.getObject(5)));
					
					set_Adresse_VIlle(String.valueOf(rs.getObject(6))); 
			}
			
		    state.close();  
		 } 
			
		catch (Exception e) 
		{
		    e.printStackTrace();
		} 	
	}

	// Setteurs & Getteurs
	
	public int getID()
	{
		return ID;
	}
	public String get_Nom()
	{
		return Nom;
	}
	public String get_Prenom()
	{
		return Prenom;
	}
	public String get_Adresse_Rue()
	{
		return Adresse_Rue;
	}
	public String get_Adresse_CP()
	{
		return Adresse_CP;
	}
	public String get_Adresse_Ville()
	{
		return Adresse_Ville;
	}
	public String get_pseudo()
	{
		return pseudo;
	}
	public String get_mot_de_passe()
	{
		return mot_de_passe;
	}
	
	public void setID(int my_id)
	{
		ID=my_id;
	}
	public void set_Nom(String myname)
	{
		Nom=myname;
	}
	public void set_Prenom(String myPrenom)
	{
		Prenom=myPrenom;
	}
	public void set_Adresse_Rue(String rue)
	{
		Adresse_Rue=rue;
	}
	public void set_Adresse_CP(String CP)
	{
		Adresse_CP=CP;
	}
	public void set_Adresse_VIlle(String city)
	{
		Adresse_Ville=city;
	}
	public void set_pseudo(String mypseudo)
	{
		pseudo=mypseudo;
	}
	public void set_mdp(String mdp)
	{
		mot_de_passe=mdp;
	}
	public void set_adresse(String rue, String CP, String city)
	{
		set_Adresse_Rue(rue);
		set_Adresse_CP(CP);
		set_Adresse_VIlle(city);
	}
}
