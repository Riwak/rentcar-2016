//
//  Vehicule.java
//  Java - RentCar_2016_Project
//
//  Created by Audouin d'Aboville on 11/03/2016.
//  Copyright (c) 2016 Audouin d'Aboville. All rights reserved.
//

import java.sql.Statement;
import java.util.Scanner;
import java.io.IOException;
import java.sql.*;

public class Vehicule 
{
	private int ID;
	private String Marque;
	private String Moto;
	private String Auto;
	private String Modele;
	private String Cylindre;
	private String Immatriculation;
	private String kilometrage;
	private String disponibilite;
	private String Carburant_level;
	private String etat;
	private String TypeClient;
	private String prix;
	
	public Vehicule(){} // Constructeur par default
	
	public void Display_Vehicule() // OK
	{ // Affichage des infos véhicule
		System.out.println("Affichage du Vehicule : " + getID());
		System.out.println("---------------------------------");
		System.out.println("Disponnibilité : " + get_dispo());
		System.out.println("\n");
		System.out.println("Marque : " + get_Marque());
		System.out.println("Modèle : " + get_Modele());
		System.out.println("Immatriculation : " + get_immatriculation());
		System.out.println("Kilometrage : " + get_kilometrage());
		System.out.println("Carburant level : " + get_carburant_level());
		System.out.println("Prix unitaire : " + get_price());
		System.out.println("Cylindre : " + get_Cylindre());
		System.out.println("\n");
		System.out.println("Etat du véhicule (%) : " + get_etat());
		
		// Affichage du type de véhicule
		if (get_TypeClient()=="1")
		{
			System.out.println("Clientèle : Luxe");
		}
		else
		{
			System.out.println("Clientèle : Standard");
		}
		
		System.out.println("---------------------------------");
	}
	
	public void recuperation_BDD_Vehicule(int id)  throws ClassNotFoundException // Accès BDD : OK
	{ // Récupération BDD et remplisage de la classe
		try 
		{   
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
		    String user = "root";
		    String passwd = "root";
		         
		    Connection conn = DriverManager.getConnection(url, user, passwd);
		         
		    //Création d'un objet Statement
		    Statement state = conn.createStatement();
		    //L'objet ResultSet contient le résultat de la requête SQL
		    ResultSet rs = state.executeQuery("SELECT * FROM Vehicule WHERE id='" + id + "'");
		    
			while (rs.next()) 
			{
					setID((int) rs.getObject(1));
					set_Marque(String.valueOf(rs.getObject(2)));
					set_Moto(String.valueOf(rs.getObject(3)));
					set_Auto(String.valueOf(rs.getObject(4)));
					set_Modele(String.valueOf(rs.getObject(5)));
					set_Cylindre(String.valueOf(rs.getObject(6)));
					set_immatriculation(String.valueOf(rs.getObject(7)));
					set_kilometrage(String.valueOf(rs.getObject(8)));
					set_disponibilite(String.valueOf(rs.getObject(9)));
					set_carburant_level(String.valueOf(rs.getObject(10)));
					set_etat(String.valueOf(rs.getObject(11)));
					setTypeClient(String.valueOf(rs.getObject(12)));
					set_price(String.valueOf(rs.getObject(13)));
			}
			
		    state.close();  
		 } 
			
		catch (Exception e) 
		{
		    e.printStackTrace();
		    // Gestion des exceptions
		} 	
	}
	
	public void create_new_vehicule() throws ClassNotFoundException // Accès BDD : OK
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Outil de création de véhicule");
		System.out.println("-----------------------------");
		System.out.println("\n");
		
		System.out.println("Saisir la marque : ");
		String Marque_tempory = sc.nextLine();
		
		System.out.println("Saisir le type (Moto=1)(Auto=2) : ");
		String type_auto_moto_tempory = sc.nextLine();
		
		String Moto;
		String Auto;
		
		if (type_auto_moto_tempory=="1")
		{
			Moto="1";
			Auto="0";
		}
		else
		{
			Auto="1";
			Moto="0";
		}
		
		System.out.println("Saisir le modèle : ");
		String Modele_tempory = sc.nextLine();
		
		System.out.println("Saisir le cylindre : ");
		String Cylindre_tempory = sc.nextLine();
		
		System.out.println("Saisir l'immatriculation : ");
		String immatriculation_tempory = sc.nextLine();
		
		System.out.println("Saisir le kilométrage (<180 000) : ");
		String kilometrage_tempory = sc.nextLine();
		
		// Mise en place d'une limitaion au kilométrage Cf Sujet
		if (Integer.valueOf(kilometrage_tempory)>180000)
		{
			kilometrage_tempory = "180000";
		}
		
		System.out.println("Saisir la disponibilité (Bool 0/1) : ");
		String disponibilit_tempory = sc.nextLine();
		
		System.out.println("Saisir le niveau de carburant (en %) : ");
		String carburant_level_tempory = sc.nextLine();
		
		System.out.println("Saisir l'etat du vehicule (en %) : ");
		String etat_tempory = sc.nextLine();
		
		System.out.println("Saisir le type de clientèle (luxe=1)(standard=2) : ");
		String clientele_tempory = sc.nextLine();
		
		System.out.println("Saisir le prix : ");
		String prix_tempory = sc.nextLine();
		
		// Accès BDD puis enregistrement des valeurs _tempory
		
		try 
		{   
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
		    String user = "root";
		    String passwd = "root";
		         
		    Connection conn = DriverManager.getConnection(url, user, passwd);
		         
		    //Création d'un objet Statement
		    Statement state = conn.createStatement();
		    //L'objet ResultSet contient le résultat de la requête SQL
		    state.executeUpdate("INSERT INTO Vehicule (Marque, Moto, Auto, Modele, Cylindre, Immatriculation, Kilometrage, Disponibilite, Carburant_level, etat, TypeClient, Prix) VALUES ('" + Marque_tempory + "', '" + Moto + "', '" + Auto + "', '" + Modele_tempory + "', '" + Cylindre_tempory + "', '" + immatriculation_tempory + "', '" + kilometrage_tempory + "', '" + disponibilit_tempory + "', '" + carburant_level_tempory + "', '" + etat_tempory + "', '" + clientele_tempory + "', '" + prix_tempory + "')");
		    state.close();      
		 } 
			
		catch (Exception e) 
		{
		    e.printStackTrace();
		} 
		
		System.out.println("----------------------");
		System.out.println("Enregistrement terminé");
		
	}
	
	public void delete_Vehicule(String id)  throws ClassNotFoundException // Accès BDD : OK
	{
		System.out.println("Utilitaire de suppression");
		System.out.println("-------------------------");
		System.out.println("Supression ID : " + id);
		int cpt=0;
		
		// Vérification + Suppression
		try 
		{   
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
		    String user = "root";
		    String passwd = "root";
		    Connection conn = DriverManager.getConnection(url, user, passwd);
		    Statement state2 = conn.createStatement();
		    ResultSet rs = state2.executeQuery("SELECT * FROM Vehicule WHERE Disponibilite=1");
		    
		    while (rs.next())
		    {
		    	cpt++;
		    }
		    
		    if (cpt==0)
		    {
		    	try 
				{   
					Class.forName("com.mysql.jdbc.Driver");
				    //Création d'un objet Statement
				    Statement state = conn.createStatement();
				    //L'objet ResultSet contient le résultat de la requête SQL
				    state.executeUpdate("DELETE FROM `Vehicule` WHERE `ID` = '" + id + "'");
				    state.close();  
				 } 
					
				catch (Exception e) 
				{
				    e.printStackTrace();
				}
		    }
		    else
		    {
		    	System.out.print("Impossible de supprimer un véhicule utilisé dans une location");
		    	System.out.println("\n");
		    }
		    
		    state2.close();  
		 } 
			
		catch (Exception e) 
		{
		    e.printStackTrace();
		}
		
		System.out.println("----------------------");
		System.out.println("Suppression terminé");
		System.out.println("\n");
	}
	
	public void edit_vehicule()  throws ClassNotFoundException // Accès BDD : OK
	{
		Scanner sc = new Scanner(System.in);
		
		// On affiche d'abord le véhicule que l'on souhaite modifier 
		Display_Vehicule();
		
		System.out.println("Outil de modification de véhicule");
		System.out.println("-----------------------------");
		System.out.println("\n");
		
		System.out.println("Modification du prix : ");
		String prix_edit_tempory = sc.nextLine();
		
		System.out.println("Modification de l'etat (en %) : ");
		String etat_edit_tempory = sc.nextLine();
		
		System.out.println("Modification du kilometrage (<180 000Km): ");
		String kilometrage_edit_tempory = sc.nextLine();
		
		// Limitation de kilométrage Cf plus haut
		if (Integer.valueOf(kilometrage_edit_tempory)>180000)
		{
			kilometrage_edit_tempory = "180000";
		}
		
		System.out.println("Carburant level (en %) : ");
		String carburant_level_edit_tempory = sc.nextLine();
		
		System.out.println("Disponibilite (0/1) ? : ");
		String disponibilite_tempory = sc.nextLine();
		
		// Accès BDD et EDIT 
		try 
		{   
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
		    String user = "root";
		    String passwd = "root";
		    
		    Connection conn = DriverManager.getConnection(url, user, passwd);
		         
		    //Création d'un objet Statement
		    Statement state = conn.createStatement();
		    //L'objet ResultSet contient le résultat de la requête SQL
		    state.executeUpdate("UPDATE `Vehicule` SET `Prix`='" + prix_edit_tempory + "' WHERE `id`='" + getID()+"'");
		    state.executeUpdate("UPDATE `Vehicule` SET `etat`='" + etat_edit_tempory + "' WHERE `id`='" + getID()+"'");
		    state.executeUpdate("UPDATE `Vehicule` SET `Kilometrage`='" + kilometrage_edit_tempory + "' WHERE `id`='" + getID()+"'");
		    state.executeUpdate("UPDATE `Vehicule` SET `Carburant_level`='" + carburant_level_edit_tempory + "' WHERE `id`='" + getID()+"'");
		    state.executeUpdate("UPDATE `Vehicule` SET `Disponibilite`='" + disponibilite_tempory + "' WHERE `id`='" + getID()+"'");
			
		    state.close();  
		 } 
			
		catch (Exception e) 
		{
		    e.printStackTrace();
		}
	}
	
	
	// Getteurs & Setteurs
	
	public int getID()
	{
		return ID;
	}
	public String get_Marque()
	{
		return Marque;
	}
	public String get_Moto()
	{
		return Moto;
	}
	public String get_Auto()
	{
		return Auto;
	}
	public String get_Modele()
	{
		return Modele;
	}
	public String get_Cylindre()
	{
		return Cylindre;
	}
	public String get_immatriculation()
	{
		return Immatriculation;
	}
	public String get_dispo()
	{
		return disponibilite;
	}
	public String get_carburant_level()
	{
		return Carburant_level;
	}
	public String get_etat()
	{
		return etat;
	}
	public String get_TypeClient()
	{
		return TypeClient;
	}
	public String get_price()
	{
		return prix;
	}
	public String get_kilometrage()
	{
		return kilometrage;
	}
	
	public void setID(int my_id)
	{
		ID=my_id;
	}
	public void set_Marque(String my_marque)
	{
		Marque=my_marque;
	}
	public void set_Moto(String value)
	{
		Moto=value;
	}
	public void set_Auto(String value)
	{
		Auto=value;
	}
	public void set_Modele(String my_modele)
	{
		Modele=my_modele;
	}
	public void set_Cylindre(String my_cylindre)
	{
		Cylindre=my_cylindre;
	}
	public void set_immatriculation(String my_immatriculation)
	{
		Immatriculation=my_immatriculation;
	}
	public void set_kilometrage(String distance)
	{
		kilometrage=distance;
	}
	public void set_disponibilite(String value)
	{
		disponibilite=value;
	}
	public void set_carburant_level(String level)
	{
		Carburant_level=level;
	}
	public void set_etat(String value)
	{
		etat=value;
	}
	public void setTypeClient(String value)
	{
		TypeClient=value;
	}
	public void set_price(String value)
	{
		prix=value;
	}

}
