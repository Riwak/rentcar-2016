//
//  Admin.java
//  Java - RentCar_2016_Project
//
//  Created by Audouin d'Aboville on 11/03/2016.
//  Copyright (c) 2016 Audouin d'Aboville. All rights reserved.
//

import java.sql.Statement;
import java.util.Date;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;

public class Admin 
{
	 private String pseudo;
	 private String mot_de_passe;
	 
	 public Admin() {} // Constructeur par default
	    
	 public Boolean init_admin(String pseudo, String mdp) // Accès BDD : OK
	 { // Fonction d'initialisation du compte administrateur
		 int cpt = 0;
		 
		 try 
		 {   
			 	Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
			    String user = "root";
			    String passwd = "root";
			         
			    Connection conn = DriverManager.getConnection(url, user, passwd);
			         
			    //Création d'un objet Statement
			    Statement state = conn.createStatement();
			    //L'objet ResultSet contient le résultat de la requête SQL
			    ResultSet rs = state.executeQuery("SELECT * FROM Admin WHERE pseudo='" + pseudo + "' AND mot_de_passe='" + mdp + "'");				
			    
				while (rs.next())
				{
					// Enregistrement des valeurs récupérés en BDD dans la classe Admin
					cpt++;
					set_pseudo(String.valueOf(rs.getObject(1)));
					set_mot_de_passe_admin(String.valueOf(rs.getObject(2)));
				}
				
			    state.close();  
			 } 
				
			catch (Exception e) 
			{
			    e.printStackTrace();
			    // gestion des exceptions 
			}
		 
		 Boolean login;
		 
		 if (cpt==0)
		 {
			// Mot de passe ou identifiant incorrect
			login = false;
		 }
		 else
		 {
			 System.out.println("---------------------------------------------");
			System.out.println("Connexion réussie");
			System.out.println("\n");
			login = true; // Variable autorisant l'accès admin
		 }
		 
		 return login;
	 }
	 
	 public void list_all_object(String type, String order, String what) throws ClassNotFoundException // Accès BDD : OK
	 { // Fonction de listing de vehicules, locations, clients
		 try
			{   
			 	Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
			    String user = "root";
			    String passwd = "root";
			         
			    Connection conn = DriverManager.getConnection(url, user, passwd);
			         
			    //Création d'un objet Statement
			    Statement state = conn.createStatement();
			    //L'objet ResultSet contient le résultat de la requête SQL
			    ResultSet rs = state.executeQuery("SELECT "+ what +" FROM "+ type +" ORDER BY "+ order +"");
			    ResultSetMetaData metaData = rs.getMetaData();
				int numberOfCloumns = metaData.getColumnCount();
				
				if (order.equals("ID"))
				{
					System.out.printf(">"+ type + " database (by "+ order +") %n%n");
				}
				
				// Affichage des tables SQL selon paramètres
				
				for (int i = 1; i <= numberOfCloumns; i++) 
				{
					System.out.printf("%-8s\t", metaData.getColumnName(i)); 
				}
			 
				System.out.println(""); 
				
				while (rs.next()) 
				{
					for (int i = 1; i <= numberOfCloumns; i++) 
					{
						System.out.printf("%-8s\t", rs.getObject(i));
					}
			 	
					System.out.println("");
				}
				
			}
		 	catch (SQLException e) 
			{
			   e.printStackTrace();
			   // Gestion d'exceptions
			}
		 
		 System.out.println("\n");
	 }
	 
	 public void display_menu_admin(Boolean login) throws NumberFormatException, ClassNotFoundException // OK
, FileNotFoundException
	 { // Affichage du menu global administrateur
		 
		 if(login==true)
		 {
			 // Variables de navigation réutilisable
			 String choix;
			 String choix2;
			 String choix3;
			 String choix4;
			 String choix0;
			 
			 // Variable de logout
			 Boolean connect = true;
			 
			 while (connect == true)
			 {
				 System.out.println("--------------- RenTCar V1 ------------------");
				 
				 System.out.println("1) Affichage vehicule(s)");
				 System.out.println("2) Affichage client(s)");
				 System.out.println("3) Affichage location(s)"); 
				 System.out.println("\n");
				 System.out.println("4) Détails vehicule");
				 System.out.println("5) Détails client");
				 System.out.println("6) Détails location");
				 System.out.println("\n");
				 System.out.println("7) Création vehicule");
				 System.out.println("8) Création client");
				 System.out.println("9) Création location");
				 System.out.println("\n");
				 System.out.println("10) Suppression vehicule");
				 System.out.println("11) Suppression client");
				 System.out.println("12) Suppression location");
				 System.out.println("\n");
				 System.out.println("13) Outil statistique"); 
				 System.out.println("14) Deconnexion");
				 
				 Scanner sc = new Scanner(System.in);
		
				 System.out.println("---------------------------------------------");
				 System.out.println("Choix : ");
				 choix = sc.nextLine();
					
				 if(choix.equals("1")) // OK
				 {
					 System.out.println("---------------------------------------------");
					 System.out.println("Affichage vehicule(s) : ");
					 
					 System.out.println("1) Recherche par Marque");
					 System.out.println("2) Recherche par Kilométrage");
					 
					 System.out.println("3) Liste Moto");
					 System.out.println("4) Liste Auto");
					 System.out.println("5) Tri Marque");
					 System.out.println("6) Tri Kilometrage");
					 
					 System.out.println("7) Affichage global by ID");
					 System.out.println("---------------");
					 System.out.println("Choix : ");
					 choix0 = sc.nextLine();
					 
					 if (choix0.equals("1"))
					 {
						 String choix9;
						 System.out.println("Entrez une marque : ");
						 choix9 = sc.nextLine();
						 
						// Surcharge du type pour la condition
						 list_all_object("Vehicule WHERE Marque='"+ choix9 +"'", "ID", "*");
					 }
					 else if (choix0.equals("2"))
					 {
						 String choix9;
						 System.out.println("Entrez un kilometrage : ");
						 choix9 = sc.nextLine();
						 
						// Surcharge du type pour la condition
						 list_all_object("Vehicule WHERE Kilometrage='"+ choix9 +"'", "ID", "*");
					 }
					 else if (choix0.equals("3"))
					 {
						// Surcharge du type pour la condition
						 list_all_object("Vehicule WHERE Moto=1", "ID", "*");
					 }
					 else if (choix0.equals("4"))
					 {
						 // Surcharge du type pour la condition
						 list_all_object("Vehicule WHERE Auto=1", "ID", "*");
					 }
					 else if (choix0.equals("5"))
					 {
						 list_all_object("Vehicule", "Marque", "*");
					 }
					 else if (choix0.equals("6"))
					 {
						 list_all_object("Vehicule", "Kilometrage", "*");
					 }
					 else // 7
					 {
						 list_all_object("Vehicule", "ID", "*");
					 }
				 }
				 else if (choix.equals("2")) // OK
				 {
					 System.out.println("---------------------------------------------");
					 System.out.println("Affichage client(s) : ");
					 
					 System.out.println("1) Recherche par Nom");
					 System.out.println("2) Recherche par Vehicule (ID Vehicule)");
					 
					 System.out.println("3) Tri Nom");
					 System.out.println("4) Tri Facture");
					 
					 System.out.println("5) Affichage global by ID");
					 System.out.println("---------------");
					 System.out.println("Choix : ");
					 choix0 = sc.nextLine();
					 
					 if (choix0.equals("1"))
					 {
						 String choix9;
						 System.out.println("Entrez un nom : ");
						 choix9 = sc.nextLine();
						 
						// Surcharge du type pour la condition
						 list_all_object("Client WHERE Nom='"+ choix9 +"'", "ID", "*");
					 }
					 else if (choix0.equals("2"))
					 {
						 String choix9;
						 System.out.println("Entrez un ID Vehicule : ");
						 choix9 = sc.nextLine();
						 
						// Surcharge du type pour la condition
						 list_all_object("Location L, Client C WHERE L.Vehicule_ID='"+ choix9 +"' AND L.ClientID=C.ID", "Nom", "Nom, Prenom, Adresse_Rue, Adresse_CP, Adresse_Ville");
					 }
					 else if (choix0.equals("3"))
					 {
						 list_all_object("Client", "Nom", "*");
					 }
					 else if (choix0.equals("4"))
					 {
						 System.out.println("Pour afficher le detail de la facture rdv sur le details location");
						 System.out.println("-----------------------------------------------------------------");
						 list_all_object("Client", "prix_periode", "*");
					 }
					 else // 5
					 {
						 list_all_object("Client", "ID", "*");
					 }
				 }
				 else if (choix.equals("3")) // OK
				 {
					 Date date = new Date();
					 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
					 String dateDuJour = sdf.format(date); 
					 
					 System.out.println("Date du jour : " + dateDuJour);
					 System.out.println("---------------------------------------------");
					 System.out.println("Affichage location(s) : ");
					
					 System.out.println("1) Recherche par date debut");
					 System.out.println("2) Recherche par location en cours");
					 
					 System.out.println("3) Affichage global by ID");
					 System.out.println("---------------");
					 System.out.println("Choix : ");
					 choix0 = sc.nextLine();
					 
					 if (choix0.equals("1"))
					 {
						 String choix9;
						 System.out.println("Entrez une date (jj/mm/aaaa) : ");
						 choix9 = sc.nextLine();
						 
						// Surcharge du type pour la condition
						 list_all_object("Location WHERE debut='"+ choix9 +"'", "ID", "*");
					 }
					 else if (choix0.equals("2"))
					 {
						 // Recherche par location en cours
						 list_all_object("Location WHERE '"+ dateDuJour +"' BETWEEN debut AND fin", "ID", "*");
					 }
					 else // 3
					 {
						 list_all_object("Location", "ID", "*");
					 }
					 
				 }
				 else if (choix.equals("4")) // OK
				 {
					 System.out.println("---------------------------------------------");
					 System.out.println("Saisir l'ID du vehicule");
					 choix2 = sc.nextLine();
					 
					 // Initialisation du vehicule en fct de l'ID et récupération des infos
					 Vehicule Vroom = new Vehicule();
					 Vroom.recuperation_BDD_Vehicule(Integer.parseInt(choix2));
					 Vroom.Display_Vehicule();
					 
					 System.out.println("Souhaitez vous modifier le vehicule ? (true/false) : ");
					 choix3 = sc.nextLine();
					 
					 if (choix3.equals("true"))
					 {
						Vroom.edit_vehicule();
					 }
				 }
				 else if (choix.equals("5")) // OK
				 {
					 System.out.println("---------------------------------------------");
					 System.out.println("Saisir l'ID du client");
					 choix3 = sc.nextLine();
					 
					 // Initialisation du client en fct de l'ID et récupération des infos 
					 Client toto = new Client();
					 toto.retrieveBDD_account(Integer.parseInt(choix3));
					 toto.display_account();
					 
					 System.out.println("Souhaitez vous modifier le client ? (true/false) : ");
					 choix2 = sc.nextLine();
					 
					 if (choix2.equals("true"))
					 {
						toto.edit_account();
					 }
				 }
				 else if (choix.equals("6")) // OK
				 {
					 System.out.println("---------------------------------------------");
					 System.out.print("Saisir l'ID de la location");
					 choix4 = sc.nextLine();
					 
					 // Initialisation de la location en fct de l'ID et récupération des infos
					 Location fiche = new Location();
					 fiche.Recuperation_BDD_Location(Integer.parseInt(choix4));
					 fiche.Affichage_Location();
					 
					 System.out.println("Souhaitez vous modifier la location ? (true/false) : ");
					 choix3 = sc.nextLine();
					 
					 if (choix3.equals("true"))
					 {
						fiche.edit_location();
					 }
					 
					 choix3="false";
					 System.out.println("Souhaitez vous génerer une facture ? (true/false) : ");
					 choix3 = sc.nextLine();
					 
					 if (choix3.equals("true"))
					 {
						fiche.Generate_facture();
					 }
				 }
				 else if (choix.equals("7")) // OK
				 {
					 System.out.println("---------------------------------------------");
					 Vehicule Vroom = new Vehicule();
					 Vroom.create_new_vehicule();
				 }
				 else if (choix.equals("8")) // OK 
				 {
					 System.out.println("---------------------------------------------");
					 Client toto = new Client();
					 toto.create_account();
				 }
				 else if (choix.equals("9")) // OK
				 {
					 System.out.println("---------------------------------------------");
					 Location fiche = new Location();
					 fiche.Create_New_Location();
				 }
				 else if (choix.equals("10")) // OK
				 {
					 System.out.println("---------------------------------------------");
					 System.out.println("Saisir l'ID du vehicule à supprimer");
					 choix2 = sc.nextLine();
					 
					 // Une vérification de sécurité est effectué au sein de la fct de suppression
					 Vehicule Vroom = new Vehicule();
					 Vroom.delete_Vehicule(choix2);
				 }
				 else if (choix.equals("11")) // OK
				 {
					 System.out.println("---------------------------------------------");
					 System.out.println("Saisir l'ID du client à supprimer");
					 choix3 = sc.nextLine();
					 
					 // Une vérification de sécurité est effectué au sein de la fct de suppression
					 Client toto = new Client();
					 toto.delete_account(choix3);
				 }
				 else if (choix.equals("12")) // OK
				 {
					 System.out.println("---------------------------------------------");
					 System.out.println("Saisir l'ID de la location à supprimer");
					 choix4 = sc.nextLine();
					 
					 // Une vérification de sécurité est effectué au sein de la fct de suppression
					 Location fiche = new Location();
					 fiche.Delete_Location(choix4);
				 }
				 else if (choix.equals("13"))
				 {
					 // Accès à la classe statistique et ses outils de gestions
					 System.out.println("---------------------------------------------");
					 Statistiques my_stat_tools = new Statistiques();
					 my_stat_tools.Display_Menu();
				 }
				 else // Deconnexion
				 {
					 connect=false; // Sortie de boucle --> Logout 
					 System.out.println("---------------------------------------------");
					 System.out.println("BYE ! ");
				 }
			 }
		 }
		 else 
		 {
			System.out.println("-------------------------------");
			System.out.println("couple idt/mdp introuvable ");
			System.out.println("Veuillez recommencer...");
		 }
	 } 
	 
	 
	 // Setteurs & Getteurs.
	 
	 public String get_pseudo_admin()
	 {
		 return pseudo;
	 }
	 public String get_mot_de_passe_admin()
	 {
		return mot_de_passe; 
	 }
	 
	 public void set_pseudo(String texte)
	 {
		 pseudo=texte;
	 }
	 public void set_mot_de_passe_admin(String texte)
	 {
		 mot_de_passe=texte;
	 }	        
}
