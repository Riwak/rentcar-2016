//
//  main.java
//  Java - RentCar_2016_Project
//
//  Created by Audouin d'Aboville on 11/03/2016.
//  Copyright (c) 2016 Audouin d'Aboville. All rights reserved.
//

import java.sql.Statement;
import java.util.Scanner;
import java.io.IOException;
import java.sql.*;

public class main 
{
	public static int mysql_test() throws IOException, ClassNotFoundException
	{ // Structure de vérification pour la BDD
		 try
			{   
			 	Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
				String user = "root";
				String passwd = "root";
				Connection conn = DriverManager.getConnection(url, user, passwd); 
				//Création d'un objet Statement
				Statement state = conn.createStatement();
				//L'objet ResultSet contient le résultat de la requête SQL
				state.executeQuery("SELECT * FROM Admin WHERE ID=2");
				//System.out.println("All OK : XX000");
			}
			catch (SQLException e) 
			{
				System.out.println("Erreur XX001");
				// Erreur de connexion à la BDD SQL Check port ou status serveur.
				e.printStackTrace();
				return 0;
			}
		 
		 return 1;
	}
	
	public static void main(String[] argv) throws NumberFormatException, ClassNotFoundException, IOException  
	{		
		// Test de connection BDD et vérification d'intégrité.
		int check;
		check = mysql_test();
		
		if (check==1)
		{
			String identifiant;
			String mot_de_passe;
			Scanner sc = new Scanner(System.in);
			
			System.out.println("--------------- RenTCar V1 ------------------");
			System.out.println("Connexion en tant qu'administrateur.");
			System.out.println("Saisir l'identifiant : ");
			identifiant = sc.nextLine();
			
			System.out.println("Saisir le mot de passe : ");
			mot_de_passe = sc.nextLine();
			
			// Initialisation du compte administrateur et lancement du programme
			Boolean login_status = false;
			Admin the_boss = new Admin();
			login_status = the_boss.init_admin(identifiant, mot_de_passe);
			the_boss.display_menu_admin(login_status);
			
			sc.close();
		}
		else
		{
			System.out.println("--------------- RenTCar V1 ------------------");
			System.out.println("Problème sur la BDD : " + check);
			// retour d'erreur personnalisé lorsqu'il y a un problème sur la BDD
		}
	}
}
