//
//  Location.java
//  Java - RentCar_2016_Project
//
//  Created by Audouin d'Aboville on 11/03/2016.
//  Copyright (c) 2016 Audouin d'Aboville. All rights reserved.
//

import java.sql.Statement;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;

public class Location 
{
	private int ID;
	private String numero;
	private String vehicule_ID;
	private String assurance;
	private String Statut_paye;
	private String prix_periode; // Durée de la période
	
	private String date_debut;
	private String date_fin;
	private String ClientID;
	
	public Location(){} // Constructeur par default
	
	public void Create_New_Location() // Accès BDD : OK
	{ // Création d'une nouvelle location
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Outil de création d'une location");
		System.out.println("--------------------------------");
		System.out.println("\n");
		
		System.out.println("Saisir une reference : ");
		String numero_tempory = sc.nextLine();

		System.out.println("Saisir l'ID du véhicule : ");
		String id_vehicule_tempory = sc.nextLine();
		
		System.out.println("Assurance (true/false) : ");
		String assurance_tempory = sc.nextLine();
		
		System.out.println("Statut de paiement (true/false) : ");
		String paiement_tempory = sc.nextLine();
		
		System.out.println("Durée de la location (en nb de jours) : ");
		String prix_periode_tempory = sc.nextLine();
		
		System.out.println("Saisir la date de début de location (jj/mm/aaaa) : ");
		String start_tempory = sc.nextLine();
		
		System.out.println("Saisir la date de fin de location (jj/mm/aaaa) : ");
		String end_tempory = sc.nextLine();
		
		System.out.println("Saisir l'ID du client : ");
		String client_tempory = sc.nextLine();
		
		// Vérification que dans une autre location il n'y pas le même ID voiture
		try 
		{   
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
		    String user = "root";
		    String passwd = "root";
		    Connection conn = DriverManager.getConnection(url, user, passwd);
		    //Création d'un objet Statement
		    Statement state = conn.createStatement();
		    Statement state2 = conn.createStatement();
		    //L'objet ResultSet contient le résultat de la requête SQL
		    ResultSet rs = state.executeQuery("SELECT Count(*) FROM Vehicule WHERE ID='" + id_vehicule_tempory + "'");
		    
			while (rs.next()) 
			{
				try 
				{   
					System.out.println("Enregistrement en cours");
					state2.executeUpdate("INSERT INTO Location (numero, debut, fin, Vehicule_ID, prix_periode, assurance, paye, ClientID) VALUES ('" + numero_tempory + "', '" + start_tempory + "', '" + end_tempory + "', '" + id_vehicule_tempory + "', '" + prix_periode_tempory + "', '" + assurance_tempory + "', '" + paiement_tempory + "', '" + client_tempory + "')");
				} 
				
				catch (Exception e) 
				{
				    e.printStackTrace();
				}
			}
		    state.close();
		 } 
			
		catch (Exception e) 
		{
		    e.printStackTrace();
		} 
		
		System.out.println("----------------------");
		System.out.println("Enregistrement terminé");
		
	}
	
	public void Recuperation_BDD_Location(int my_id) throws ClassNotFoundException // Accès BDD : OK
	{ // Initialisation de la classe Location avec ses infos BDD
		try 
		{   
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
		    String user = "root";
		    String passwd = "root";
		         
		    Connection conn = DriverManager.getConnection(url, user, passwd);
		         
		    //Création d'un objet Statement
		    Statement state = conn.createStatement();
		    //L'objet ResultSet contient le résultat de la requête SQL
		    ResultSet rs = state.executeQuery("SELECT * FROM Location WHERE id='" + my_id + "'");
		    
			while (rs.next()) 
			{
					setID((int) rs.getObject(1)); // Cast string -> int necessaire
					set_numero(String.valueOf(rs.getObject(2)));
					set_date_start(String.valueOf(rs.getObject(3)));
					set_date_fin(String.valueOf(rs.getObject(4)));
					set_vehicule_ID(String.valueOf(rs.getObject(5)));
					set_prix_periode(String.valueOf(rs.getObject(6)));
					set_assurance(String.valueOf(rs.getObject(7)));
					set_statut_paye(String.valueOf(rs.getObject(8)));
					set_ClientID(String.valueOf(rs.getObject(9)));
			}
			
		    state.close();  
		 } 
			
		catch (Exception e) 
		{
		    e.printStackTrace();
		    // Gestion des exceptions
		}
	}
	
	public void edit_location() throws ClassNotFoundException // Accès BDD : OK
, FileNotFoundException
	{
		Scanner sc = new Scanner(System.in);
		
		Affichage_Location();
		
		System.out.println("Outil de modification de location");
		System.out.println("---------------------------------");
		System.out.println("\n");
		System.out.println("1) Penalité Retard");
		System.out.println("2) Pénalité Carburant");
		System.out.println("3) Pénalité Etat");
		System.out.println("4) Modification globale");
		System.out.println("---------------------------------");
		System.out.println("Choix : ");
		String choix = sc.nextLine();
		
		if (choix.equals(1))
		{
			// Ajout des pénalités de retard (JR)
			System.out.println("---------------------------------");
			System.out.println("Combien de jours de retard : ");
			String nombre = sc.nextLine();
			
			int Calcul = Integer.parseInt(get_prix_periode())+Integer.parseInt(nombre);
			set_prix_periode(Integer.toString(Calcul));
			
			try 
			{   // Mise à jour de la BDD en conséquence
				Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
			    String user = "root";
			    String passwd = "root";
			    Connection conn = DriverManager.getConnection(url, user, passwd);
			    Statement state = conn.createStatement();
			    state.executeUpdate("UPDATE `Location` SET `prix_periode`='" + get_prix_periode() + "' WHERE `id`='" + getID()+"'");
			    state.close();  
			 } 
				
			catch (Exception e) 
			{
			    e.printStackTrace();
			    // Gestion des exceptions
			}
		}
		
		else if (choix.equals(2))
		{
			// Ajout de pénalité de carburant (par palier successifs)
			System.out.println("---------------------------------");
			System.out.println("Level à rajouter (25, 50, 75, 100) : ");
			String prix = sc.nextLine();
			// La pénalité de carburant se fait par level : 25%=1jr de pénalité, 50%=2, 75%=3 etc..
			int Calcul = Integer.parseInt(get_prix_periode())+(Integer.parseInt(prix)/25);
			set_prix_periode(Integer.toString(Calcul));
			
			try 
			{   // Mise à jour de la BDD en conséquences
				Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
			    String user = "root";
			    String passwd = "root";
			    Connection conn = DriverManager.getConnection(url, user, passwd);
			    Statement state = conn.createStatement();
			    state.executeUpdate("UPDATE `Location` SET `prix_periode`='" + get_prix_periode() + "' WHERE `id`='" + getID()+"'");
			    state.close();  
			 } 
				
			catch (Exception e) 
			{
			    e.printStackTrace();
			    // Gestion des exceptions
			}
		}
		
		else if (choix.equals(3))
		{
			// Ajout de pénalité degats (pas possible si assurance souscrite)
			if(get_assurance_statut().equals("true"))
			{
				System.out.println("Souscription d'une assurance: pas de frais pour l'emprunteur");
			}
			else
			{
				System.out.println("---------------------------------");
				System.out.println("Ajout d'un prix : ");
				String prix2 = sc.nextLine();
				// On compte les frais de réparations en jours d'immobilisation (a la charge de l'emprunteur)
				int Calcul = Integer.parseInt(get_prix_periode())+(Integer.parseInt(prix2)/10);
				set_prix_periode(Integer.toString(Calcul));
				
				try 
				{   // Mise à jour de la BDD en conséquences
					Class.forName("com.mysql.jdbc.Driver");
					String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
				    String user = "root";
				    String passwd = "root";
				    Connection conn = DriverManager.getConnection(url, user, passwd);
				    Statement state = conn.createStatement();
				    state.executeUpdate("UPDATE `Location` SET `prix_periode`='" + get_prix_periode() + "' WHERE `id`='" + getID()+"'");
				    state.close();  
				 } 
					
				catch (Exception e) 
				{
				    e.printStackTrace();
				}
			}
		}
		
		else // Modification sans pénalité
		{
			System.out.println("Modification du véhicule (ID) : ");
			String vehicule_edit_tempory = sc.nextLine();
			
			System.out.println("Modification du client (ID) : ");
			String client_edit_tempory = sc.nextLine();
			
			System.out.println("Modification de la durée de location : ");
			String prix_periode_edit_tempory = sc.nextLine();
			
			System.out.println("Modification de la date de début (jj/mm/aaaa) : ");
			String start_edit_tempory = sc.nextLine();
			
			System.out.println("Modification de la date de fin (jj/mm/aaaa) : ");
			String end_edit_tempory = sc.nextLine();
			
			System.out.println("Modification du statut de paiement (true/false) : ");
			String paiement_edit_tempory = sc.nextLine();
			
			System.out.println("Modification de l'assurance (true/false) : ");
			String assurance_edit_tempory = sc.nextLine();
			
			// Accès BDD et UPDATE EDIT
			try 
			{   
				Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
			    String user = "root";
			    String passwd = "root";
			    
			    Connection conn = DriverManager.getConnection(url, user, passwd);
			         
			    //Création d'un objet Statement
			    Statement state = conn.createStatement();
			    
			    //L'objet ResultSet contient le résultat de la requête SQL
			    state.executeUpdate("UPDATE `Location` SET `ClientID`='" + client_edit_tempory + "' WHERE `id`='" + getID() + "'");
			    state.executeUpdate("UPDATE `Location` SET `debut`='" + start_edit_tempory + "' WHERE `id`='" + getID() + "'");
			    state.executeUpdate("UPDATE `Location` SET `fin`='" + end_edit_tempory + "' WHERE `id`='" + getID()+"'");
			    state.executeUpdate("UPDATE `Location` SET `Vehicule_ID`='" + vehicule_edit_tempory + "' WHERE `id`='" + getID()+"'");
			    state.executeUpdate("UPDATE `Location` SET `prix_periode`='" + prix_periode_edit_tempory + "' WHERE `id`='" + getID()+"'");
			    state.executeUpdate("UPDATE `Location` SET `assurance`='" + assurance_edit_tempory + "' WHERE `id`='" + getID()+"'");
			    state.executeUpdate("UPDATE `Location` SET `paye`='" + paiement_edit_tempory + "' WHERE `id`='" + getID()+"'");
			    
			    state.close();  
			 } 
				
			catch (Exception e) 
			{
			    e.printStackTrace();
			}
		}
		
		System.out.println("----------------------");
		System.out.println("Modification terminée");
		System.out.println("----------------------");
		System.out.println("\n");
		
		Generate_facture(); // Génération automatique d'une facture après l'édition d'une location
	}
	
	public void Delete_Location(String id) // Accès BDD : OK
	{
		System.out.println("Utilitaire de suppression");
		System.out.println("-------------------------");
		System.out.println("Supression ID : " + id);
		
		try 
		{   // Attention
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:8889/RentCar_2016_project";
		    String user = "root";
		    String passwd = "root";
		    
		    Connection conn = DriverManager.getConnection(url, user, passwd);
		         
		    //Création d'un objet Statement
		    Statement state = conn.createStatement();
		    //L'objet ResultSet contient le résultat de la requête SQL
		    state.executeUpdate("DELETE FROM `Location` WHERE `ID` = '" + id + "'");
		    state.close();  
		 } 
			
		catch (Exception e) 
		{
		    e.printStackTrace();
		}
		
		System.out.println("----------------------");
		System.out.println("Suppression terminé");
	}
	
	public void Generate_facture() throws ClassNotFoundException // OK
, FileNotFoundException
	{	
		// Affichage + generation facture
		// Initialisations des composantes de la factures
		
		Vehicule Vroom = new Vehicule();
		Vroom.recuperation_BDD_Vehicule(Integer.parseInt(get_vehicule_ID()));
		
		Client Toto = new Client();
		Toto.retrieveBDD_account(Integer.parseInt(getClientID()));
		
		FirstPdf Facture = new FirstPdf();
		
		double total;
		System.out.println("Facture n : " + getID());
		System.out.println("A l'attention de " + Toto.get_Prenom() + " " + Toto.get_Nom());
		System.out.println("> début : " + get_date_debut());
		System.out.println("> fin   : " + get_date_fin());
		System.out.println("Durée de la période de location : " + get_prix_periode() + " jours");
		System.out.println("-----------------------------------------------------");
		System.out.println("Intitulé du modèle | Quantité | Prix unitaire | Total");
		System.out.println(Vroom.get_Modele() + " " + Vroom.get_Marque() + " | 1 | " + Vroom.get_price() + " | " + Integer.parseInt(Vroom.get_price())*Integer.parseInt(get_prix_periode()));
		
		total=Integer.parseInt(Vroom.get_price())*Integer.parseInt(get_prix_periode());
		
		// Prise en charge de l'assurance dans le calcul global
		if (get_assurance_statut().equals("true"))
		{
			if(Vroom.get_TypeClient().equals("1"))
			{
				System.out.println("Assurance : OUI | 100");
				System.out.println("Total HT : " + total);
				total = total+total*0.20; // Ajout TVA 20% inchangeable
				total=total+100;
			}
			else
			{
				System.out.println("Assurance : OUI | 50");
				System.out.println("Total HT : " + total);
				total = total+total*0.20; // Ajout TVA 20% inchangeable
				total=total+50;
			}
			
			Facture.generatePdf(ID, Toto, Vroom, get_date_debut(), get_date_fin(), get_prix_periode(), true, total);
		}
		else
		{
			System.out.println("Assurance : NON | 0");
			System.out.println("Total HT : " + total);
			total = total+total*0.20; // Ajout TVA 20% inchangeable
			Facture.generatePdf(ID, Toto, Vroom, get_date_debut(), get_date_fin(), get_prix_periode(), false, total);
		}
		
		System.out.println("Total TTC : " + total);
		System.out.println("-----------------------------------------------------");
	}
	
	public void Affichage_Location() throws ClassNotFoundException // OK
	{
		// Même procédé que pour la génération de la facture avec différents détails
		Vehicule Vroom = new Vehicule();
		Vroom.recuperation_BDD_Vehicule(Integer.parseInt(get_vehicule_ID()));
		
		Client Toto = new Client();
		Toto.retrieveBDD_account(Integer.parseInt(getClientID()));
		
		System.out.println("Location numéro : " + get_numero());
		System.out.println("---------------------------------");
		System.out.println("> début : " + get_date_debut());
		System.out.println("> fin   : " + get_date_fin());
		System.out.println("Durée de la période de location : " + get_prix_periode() + " jours");
		System.out.println("\n");
		System.out.println("Véhicule : " + Vroom.get_Marque() + " " + Vroom.get_Modele());
		System.out.println("Client : " + Toto.get_Prenom() + " " + Toto.get_Nom());
		
		if (get_assurance_statut().equals("true"))
		{
			System.out.println("Assurance : OUI");
		}
		else
		{
			System.out.println("Assurance : NON");
		}
		
		if (get_paye_statut().equals("true"))
		{
			System.out.println("Montant : " + Integer.parseInt(Vroom.get_price())*Integer.parseInt(get_prix_periode()));
			System.out.println("Réglé : OUI");
		}
		else
		{
			System.out.println("Montant : " + Integer.parseInt(Vroom.get_price())*Integer.parseInt(get_prix_periode()));
			System.out.println("Réglé : NON");
		}
		
		System.out.println("---------------------------------");	
	}
	
	// Getteurs & Setteurs
	
	public String getClientID()
	{
		return ClientID;
	}
 	public int getID()
	{
		return ID;
	}
	public String get_numero()
	{
		return numero;
	}
	public String get_vehicule_ID()
	{
		return vehicule_ID;
	}
	public String get_assurance_statut()
	{
		return assurance;
	}
	public String get_paye_statut()
	{
		return Statut_paye;
	}
	public String get_prix_periode()
	{
		return prix_periode;
	}
	public String get_date_debut()
	{
		return date_debut;
	}
	public String get_date_fin()
	{
		return date_fin;
	}
	
	public void set_ClientID(String my_id)
	{
		ClientID=my_id;
	}
	public void setID(int my_id)
	{
		ID=my_id;
	}
	public void set_numero(String my_number)
	{
		numero=my_number;
	}
	public void set_vehicule_ID(String v_ID)
	{
		vehicule_ID=v_ID;
	}
	public void set_assurance(String a)
	{
		assurance=a;
	}
	public void set_statut_paye(String a)
	{
		Statut_paye=a;
	}
	public void set_prix_periode(String prix)
	{
		prix_periode=prix;
	}
	public void set_date_start(String my_date)
	{
		date_debut=my_date;
	}
	public void set_date_fin(String my_date)
	{
		date_fin=my_date;
	}

}
