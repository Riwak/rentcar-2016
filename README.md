# README #

Le but de ce projet est de réaliser une application Orientée Objet pour la gestion d'une flotte de véhicules de location. Seule la partie backOffice sera traité. 
Cependant le code à été adapté pour permettre une évolution et la création d'un front-Office client.

### Version ###

* Version 1.0
* Codé en JAVA
* BDD sur MySQL

### Contribution guidelines ###

* Audouin d'Aboville

### Infos ###

EFREI : Projet JAVA - L3 - 2016